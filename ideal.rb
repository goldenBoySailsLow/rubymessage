####################################################### Michaels Solution

require 'date'

class User
  @@all = []
  attr_accessor :username,:inbox
  def initialize(username)
    @username = username
    @inbox = {'Read'=>[],'Unread'=>[]}
    @@all << self
  end

  def send_message(text, recipient)
    msg = Message.new(text)
    msg.who['to'] = recipient.username
    msg.who['from'] = self.username
    recipient.inbox['Unread'] << msg.content
    puts "Message Sent!"
  end

  def self.find(username)
    obj = @@all.select{|user| user.username == username}
    obj.first
  end
end

class Message
  @@msgs = 0
  @@all = []
  attr_accessor :content,:who
  def initialize(content)
    @id = msgs + 1
    @content = content
    @who = {'to'=>'','from'=>''}
    @sent_at = DateTime.now
    @read = false
    @@msgs += 1
    @@all << self
  end

  def self.find(id)
    obj = @@all.select{|msg| msg.id == id}
    obj.first
  end

end

user1 = User.new('mlaw')
user2 = User.new('jsmir')

user1.send_message("#{user1.username}: Not gonna lie, I didn't hear a damn thing Michael said today. Can I see your notes?",user2)
user1.send_message("#{user2.username}: me 2",user1)

p user2.inbox

m1 = Message.find(2)
puts "Found Message:#{m1.content}"
puts Message.all
