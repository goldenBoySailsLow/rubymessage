class User
  attr_accessor :received_messages,:sent_messages
  def initialize(name)
    @name = name
    @received_messages = []
    @sent_messages = []
  end

  def send(str,user)
    @sent_messages << [str,Time.now.inspect]
    user.received_messages << [str,Time.now.inspect]
  end
end
